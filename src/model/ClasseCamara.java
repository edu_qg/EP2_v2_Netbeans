package model;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.util.List;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

public class ClasseCamara implements Runnable{
    private Camara listaCompleta;  
    private List<Deputado> deputadosEmLista;
   
    public ClasseCamara(){
        listaCompleta = new Camara();
        deputadosEmLista = null;
        run();
        deputadosEmLista = listaCompleta.getDeputados();
    }

    public void run(){  
        try{listaCompleta.obterDados();}
        catch(JAXBException | IOException jaxbException){}
    }

    public List<Deputado> getList(){
        if(deputadosEmLista==null){
            System.out.println("A lista e nula");
            System.exit(1);
            return null;
        }
        else{
            return deputadosEmLista;
        } 
    }  
}
